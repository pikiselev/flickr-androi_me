package android.faiterus.com.flickr_firststeps;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface APIFlikrService {
    String getMethod = "method=flickr.interestingness.getList&api_key=13cff7b1cf2f770df1ec27aa6f1186bb";

    @GET(getMethod)
    Call<List<PhotoFlickr>> photos();
}

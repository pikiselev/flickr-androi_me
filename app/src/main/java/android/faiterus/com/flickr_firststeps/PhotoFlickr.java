package android.faiterus.com.flickr_firststeps;

public class PhotoFlickr {
    private int flickrId, server, farm;
    private boolean isPublic, isFriend, isFamily;
    private String owner, secret, title;



    public int getFlickrId() {
        return flickrId;
    }

    public int getServer() {
        return server;
    }

    public int getFarm() {
        return farm;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public boolean isFriend() {
        return isFriend;
    }

    public boolean isFamily() {
        return isFamily;
    }

    public String getOwner() {
        return owner;
    }

    public String getSecret() {
        return secret;
    }

    public String getTitle() {
        return title;
    }

}
